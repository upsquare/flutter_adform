import 'package:flutter/material.dart';
import 'package:flutteradform/adform_adinline.dart';
import 'package:flutteradform/adform_event.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Adform plugin example api'),
        ),
        body: Center(
          child: AdformAdInline(
              masterTagId: 142493,
              width: 300,
              height: 250,
              listener: (AdformEvent event, Map<String, dynamic> args) {
                switch (event) {
                  case AdformEvent.onAdLoadSuccess:
                    print("Ad successfully loaded");
                    break;
                  case AdformEvent.onAdLoadFail:
                    print("Ad load failed ${args["error"]}");
                    break;
                  default:
                }
              }),
        ),
      ),
    );
  }
}
