import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'adform_adinline_controller.dart';
import 'adform_event.dart';

class AdformAdInline extends StatefulWidget {
  final int masterTagId;
  final double width;
  final double height;
  final Function(AdformEvent, Map<String, dynamic>) listener;
  static const ADFORM_INLINE_CHANNEL = "flutter_adform/adInline";
  static const ARG_MASTER_TAG_ID = "masterTagId";
  static const ARG_WIDTH = "width";
  static const ARG_HEIGHT = "height";

  AdformAdInline(
      {Key key,
      @required this.masterTagId,
      @required this.width,
      @required this.height,
      this.listener})
      : super(key: key);

  @override
  _AdformAdInlineState createState() => _AdformAdInlineState();
}

class _AdformAdInlineState extends State<AdformAdInline>
    with WidgetsBindingObserver {
  UniqueKey _key = UniqueKey();
  AdformAdInlineController _controller;
  Future<Size> adSize;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    adSize = Future.value(Size(
      widget.width,
      widget.height,
    ));
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // call lifecycle methods only on Android
    if (defaultTargetPlatform == TargetPlatform.android) {
      if (state == AppLifecycleState.resumed) {
        _controller.resumed();
      } else if (state == AppLifecycleState.paused) {
        _controller.paused();
      } else if (state == AppLifecycleState.detached) {
        _controller.detached();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Size>(
      // applies initial future size if null or no data is present
      future: adSize,
      builder: (context, snapshot) {
        final adSize = snapshot.data;
        if (adSize == null) {
          return SizedBox.shrink();
        }

        if (defaultTargetPlatform == TargetPlatform.android) {
          return SizedBox.fromSize(
            size: adSize,
            child: AndroidView(
              key: _key,
              viewType: AdformAdInline.ADFORM_INLINE_CHANNEL,
              creationParams: <String, dynamic>{
                AdformAdInline.ARG_MASTER_TAG_ID: widget.masterTagId,
                AdformAdInline.ARG_WIDTH: widget.width.toInt(),
                AdformAdInline.ARG_HEIGHT: widget.height.toInt()
              },
              creationParamsCodec: const StandardMessageCodec(),
              onPlatformViewCreated: _onPlatformViewCreated,
            ),
          );
        } else if (defaultTargetPlatform == TargetPlatform.iOS) {
          return SizedBox.fromSize(
            size: adSize,
            child: UiKitView(
              key: _key,
              viewType: AdformAdInline.ADFORM_INLINE_CHANNEL,
              creationParams: <String, dynamic>{
                AdformAdInline.ARG_MASTER_TAG_ID: widget.masterTagId,
                AdformAdInline.ARG_WIDTH: widget.width.toInt(),
                AdformAdInline.ARG_HEIGHT: widget.height.toInt()
              },
              creationParamsCodec: const StandardMessageCodec(),
              onPlatformViewCreated: _onPlatformViewCreated,
            ),
          );
        }
        return Text(
            '$defaultTargetPlatform is not yet supported by the plugin');
      },
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    _controller.dispose();
  }

  void _onPlatformViewCreated(int id) {
    _controller = AdformAdInlineController(id, widget.listener);
  }
}
