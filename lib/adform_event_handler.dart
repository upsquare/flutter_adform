import 'package:flutter/services.dart';

import 'adform_event.dart';

abstract class AdformEventHandler {
  final Function(AdformEvent, Map<String, dynamic>) _listener;

  AdformEventHandler(Function(AdformEvent, Map<String, dynamic>) listener)
      : _listener = listener;

  Future<dynamic> handleEvent(MethodCall call) async {
    switch (call.method) {
      case 'onAdLoadFail':
        _listener(AdformEvent.onAdLoadFail,
            Map<String, dynamic>.from(call.arguments));
        break;
      case 'onAdLoadSuccess':
        _listener(AdformEvent.onAdLoadSuccess, null);
        break;
    }

    return null;
  }
}
