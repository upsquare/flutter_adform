import 'package:flutter/services.dart';

import 'adform_adinline.dart';
import 'adform_event_handler.dart';
import 'adform_event.dart';

class AdformAdInlineController extends AdformEventHandler {
  final MethodChannel _channel;

  static const RESUMED = "resumed";
  static const PAUSED = "paused";
  static const DETACHED = "detached";
  static const DISPOSE = "dispose";
  static const SET_AD_LISTENER = "setAdListener";

  AdformAdInlineController(
      int id, Function(AdformEvent, Map<String, dynamic>) listener)
      : _channel = MethodChannel("${AdformAdInline.ADFORM_INLINE_CHANNEL}_$id"),
        super(listener) {
    if (listener != null) {
      _channel.setMethodCallHandler(handleEvent);
      _channel.invokeMethod(SET_AD_LISTENER);
    }
  }

  void resumed() {
    _channel.invokeMethod(RESUMED);
  }

  void paused() {
    _channel.invokeMethod(PAUSED);
  }

  void detached() {
    _channel.invokeMethod(DETACHED);
  }

  void dispose() {
    _channel.invokeMethod(DISPOSE);
  }
}
