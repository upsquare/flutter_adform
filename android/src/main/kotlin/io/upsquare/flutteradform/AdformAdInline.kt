package io.upsquare.flutteradform

import android.content.Context
import android.view.View
import com.adform.sdk.interfaces.AdListener
import com.adform.sdk.pub.views.AdInline
import com.adform.sdk.utils.AdSize
import com.adform.sdk.utils.SmartAdSize
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.platform.PlatformView
import java.util.*

private const val RESUMED = "resumed"
private const val PAUSED = "paused"
private const val DETACHED = "detached"
private const val DISPOSE = "dispose"
private const val SET_AD_LISTENER = "setAdListener"
private const val ARG_MASTER_TAG_ID = "masterTagId"
private const val ARG_WIDTH = "width"
private const val ARG_HEIGHT = "height"
private const val EVENT_ON_AD_LOAD_FAIL = "onAdLoadFail"
private const val EVENT_ON_AD_LOAD_SUCCESS = "onAdLoadSuccess"

class AdformAdInline(context: Context, messenger: BinaryMessenger, id: Int, args: HashMap<*, *>?) : PlatformView, MethodChannel.MethodCallHandler {
    private val channel: MethodChannel = MethodChannel(messenger, "${FlutterAdformPlugin.ADFORM_ADINLINE_VIEW_TYPE}_$id")
    private val adView: AdInline = AdInline(FlutterAdformPlugin.activity)

    init {
        channel.setMethodCallHandler(this)

        val width = args?.get(ARG_WIDTH) as Int?
        val height = args?.get(ARG_HEIGHT) as Int?
        adView.masterTagId = args?.get(ARG_MASTER_TAG_ID) as Int
        if (width != null && height != null) {
            adView.adSize = AdSize(width, height)
        } else {
            adView.adSize = SmartAdSize()
        }
        adView.loadAd()
        adView.onResume()
    }

    override fun getView(): View {
        return adView
    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            SET_AD_LISTENER -> adView.setListener(createAdListener(channel))
            RESUMED -> adView.onResume()
            PAUSED -> adView.onPause()
            DETACHED -> adView.destroy()
            DISPOSE -> dispose()
            else -> result.notImplemented()
        }
    }

    override fun dispose() {
        adView.visibility = View.GONE
        adView.destroy()
        channel.setMethodCallHandler(null)
    }

    // Methods

    private fun createAdListener(channel: MethodChannel): AdListener {
        return object : AdListener {
            override fun onAdLoadFail(p0: AdInline?, error: String?) = channel.invokeMethod(EVENT_ON_AD_LOAD_FAIL, hashMapOf("error" to error))

            override fun onAdLoadSuccess(p0: AdInline?) = channel.invokeMethod(EVENT_ON_AD_LOAD_SUCCESS, null)
        }
    }
}