## 0.0.1

* Implemented AdInline ad.

## 0.0.2

* Implemented AdInline on ad load listener.

## 0.0.3

* Fixed iOS build.
